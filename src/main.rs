mod context;
mod widgets;

use widgets::Dragger;
use widgets::Button;

fn main() {
    let mut context = context::UiContext::new(640, 480)
        .expect("failed to create UI Context");

    let button = Button::new(((0.0, 0.0), (10.0, 10.0)), "Hello!".to_owned(), Box::new(|_ev, _dat| {
    }));
    let button2 = Button::new(((0.0, 0.0), (10.0, 10.0)), "Hello!".to_owned(), Box::new(|_ev, _dat| {
    }));

    let dragger = Dragger::new(button, button2);
    context.register(dragger);

    context.run_loop();
}
