use glutin::{GlContext, GlWindow};
use glutin::dpi::{PhysicalSize, LogicalSize};

use std::rc::Rc;
use std::cell::RefCell;

use super::interact::Size;

use super::databox::DataBox;

pub struct RenderContext {
    gl_wind:     GlWindow,
    renderables: Vec<Rc<RefCell<dyn Renderable>>>,

    size: PhysicalSize,
    pub dpi: f64,
}

impl RenderContext {
    pub fn new(gl_wind: GlWindow) -> Self {
        unsafe {
            gl_wind.make_current()
                .unwrap();

            gl::load_with(|symbol| gl_wind.get_proc_address(symbol) as *const _);
            gl::ClearColor(0.1, 0.1, 0.1, 1.0);
        }

        let dpi = gl_wind.get_hidpi_factor();

        let inner_size = gl_wind.get_inner_size().unwrap();
        let mut ret = Self {
            renderables: Vec::new(),
           
            size: inner_size.to_physical(dpi),
            dpi: dpi,
            gl_wind: gl_wind
        };

        ret.resize(inner_size);
        ret
    }

    pub fn tick(&mut self, context: &nanovg::Context, data: &mut DataBox) {
        unsafe {
            gl::Clear(
                gl::COLOR_BUFFER_BIT |
                gl::DEPTH_BUFFER_BIT |
                gl::STENCIL_BUFFER_BIT
            );
        }

        if data.cursor.is_some() {
            self.gl_wind.set_cursor(data.cursor.unwrap());
        }

        context.frame(
            (self.size.width as f32, self.size.height as f32),
            self.dpi as f32,
            |mut frame| {
                for renders in self.renderables.iter() {
                    (*renders).borrow().render(&mut frame, &data);
                }
        });

        self.gl_wind.swap_buffers().unwrap();
    }

    pub fn resize(&mut self, size: LogicalSize) {
        self.size = size.to_physical(self.dpi);
        self.gl_wind.resize(self.size);

        let size = self.gl_wind.get_inner_size()
            .unwrap()
            .to_physical(self.dpi);

        unsafe {
            gl::Viewport(0, 0, size.width as i32, size.height as i32);
        }

        for item in self.renderables.iter() {
            item.borrow_mut().resize(size.into())
        }
    }

    pub fn register(&mut self, item: Rc<RefCell<dyn Renderable>>) {
        self.renderables.push(item);
    }
}

pub trait Renderable {
    fn render(&self, frame: &mut nanovg::Frame, data: &DataBox);
    fn resize(&mut self, space: Size);
}
