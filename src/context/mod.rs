pub mod render;
pub mod interact;
pub mod databox;

pub use databox::DataBox;

use glutin::{EventsLoop};
use glutin::dpi::{LogicalSize};

use interact::Interactable;
use render::Renderable;

use std::rc::Rc;
use std::cell::RefCell;

pub struct UiContext {
    events: EventsLoop,

    ncontext: nanovg::Context,
    rcontext: render::RenderContext,
    icontext: interact::InteractContext
}

impl UiContext {
    pub fn new(w: u32, h: u32) -> Result<Self, Box<dyn std::error::Error>> {
        let events = glutin::EventsLoop::new();

        let window = glutin::WindowBuilder::new()
            .with_title("UI")
            .with_dimensions(LogicalSize::new(w as f64, h as f64));

        let context = glutin::ContextBuilder::new()
            .with_multisampling(4)
            .with_srgb(true);

        let gl_wind = glutin::GlWindow::new(window, context, &events)?;

        let rcontext = render::RenderContext::new(gl_wind);
        let ncontext = nanovg::ContextBuilder::new()
            .build()
            .expect("failed to create nanovg context");

        Ok(UiContext {
            events: events,

            ncontext: ncontext,
            rcontext: rcontext,
            icontext: interact::InteractContext::new()
        })
    }

    pub fn run_loop(&mut self) {
        let mut data = Box::new(DataBox::new(&self.ncontext, self.rcontext.dpi));

        let mut event_buff = Vec::new();
        loop {
            self.events.poll_events(|event| {
                event_buff.push(event);
            });

            for event in event_buff.iter() {
                match event {
                    glutin::Event::WindowEvent{event, ..} => match event {
                        glutin::WindowEvent::Resized(siz) => 
                            self.rcontext.resize(siz.clone()),
                        glutin::WindowEvent::CloseRequested =>
                            return,
                        _ => ()
                    },
                    _ => ()
                }
            }

            self.icontext.tick(event_buff.drain(..).collect(), &mut data);
            self.rcontext.tick(&self.ncontext, &mut data);
        }
    }

    pub fn register<T: Interactable + Renderable + 'static>(&mut self, item: T) {
        let item = Rc::new(RefCell::new(item));

        self.icontext.register(item.clone());
        self.rcontext.register(item);
    }
}
