use winit::MouseCursor;

lazy_static::lazy_static!{
    pub static ref DEFAULT_FONT: &'static [u8] = 
        include_bytes!("Ubuntu-R.ttf");
}

pub struct DataBox<'a> {
    pub font: nanovg::Font<'a>,
    pub cursor: Option<MouseCursor>,
    pub dpi: f64,
}

impl<'a> DataBox<'a> {
    pub fn new(cont: &'a nanovg::Context, dpi: f64) -> Self {
        let font = nanovg::Font::from_memory(&cont, "default", &DEFAULT_FONT)
            .unwrap();

        DataBox {
            font:   font,
            cursor: None,
            dpi:    dpi
        }
    }
}
