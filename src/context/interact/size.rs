use super::Vec2d;

#[derive(Copy, Clone, Debug, Default)]
pub struct Size {
    pub pos: Vec2d,
    pub siz: Vec2d
}

impl Size {
    pub fn is_inside(&self, pos: &Vec2d) -> bool {
        pos.x > self.pos.x &&
        pos.y > self.pos.y &&
        pos.x < self.pos.x + self.siz.x &&
        pos.y < self.pos.y + self.siz.y
    }
}

impl Into<((f64, f64), (f64, f64))> for Size {
    fn into(self) -> ((f64, f64), (f64, f64)) {
        (self.pos.into(),
         self.siz.into())
    }
}

impl From<((f32, f32), (f32, f32))> for Size {
    fn from(val: ((f32, f32), (f32, f32))) -> Self {
        Size {
            pos: val.0.into(),
            siz: val.1.into()
        }
    }
}

impl From<winit::dpi::PhysicalSize> for Size {
    fn from(size: winit::dpi::PhysicalSize) -> Self {
        Size {
            pos: Default::default(),
            siz: Vec2d {
                x: size.width as f32,
                y: size.height as f32
            }
        }
    }
}
