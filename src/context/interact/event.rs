use super::Vec2d;

#[derive(Debug, Clone, PartialEq)]
pub enum Event {
    MouseEnter(Vec2d),
    MouseMove(Vec2d),
    MouseLeave,
    MouseLift(Vec2d),
    MouseClick(Vec2d),
}
