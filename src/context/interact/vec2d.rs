#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub struct Vec2d {
    pub x: f32,
    pub y: f32
}

impl std::ops::Mul for &Vec2d {
    type Output = Vec2d;

    fn mul(self, &mul: Self) -> Self::Output {
        Vec2d {
            x: self.x * mul.x,
            y: self.y * mul.y
        }
    }
}

impl Into<(f64, f64)> for Vec2d {
    fn into(self) -> (f64, f64) {
        (self.x as f64, self.y as f64)
    }
}

impl Into<(f32, f32)> for Vec2d {
    fn into(self) -> (f32, f32) {
        (self.x as f32, self.y as f32)
    }
}

impl From<glutin::dpi::PhysicalPosition> for Vec2d {
    fn from(from: glutin::dpi::PhysicalPosition) -> Self {
        Vec2d {
            x: from.x as f32,
            y: from.y as f32
        }
    }
}

impl From<(f32, f32)> for Vec2d {
    fn from(from: (f32, f32)) -> Self {
        Vec2d {
            x: from.0,
            y: from.1
        }
    }
}
