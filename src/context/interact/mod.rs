pub mod size;
pub mod vec2d;
pub mod event;

pub use size::*;
pub use vec2d::*;
pub use event::*;

use std::cell::RefCell;

use super::DataBox;

use glutin::{
    ModifiersState,
    dpi::LogicalPosition,
    WindowEvent::*,
    ElementState,
    MouseButton,
};

use std::rc::Rc;

pub trait Interactable {
    fn get_bounds(&self) -> Size;

    fn is_inside(&self, pos: Vec2d) -> bool {
        let bounds = self.get_bounds();
        
        pos.x > bounds.pos.x &&
        pos.y > bounds.pos.y &&
        pos.x < bounds.pos.x + bounds.siz.x &&
        pos.y < bounds.pos.y + bounds.siz.y
    }

    fn event(&mut self, ev: Event, db: &mut Box<DataBox>);
}

pub struct InteractContext {
    interactables: Vec<Rc<RefCell<dyn Interactable>>>,
    focused: usize,

    mouse_pos:   Vec2d,
    mouse_state: ElementState,
}

impl InteractContext {
    pub fn new() -> Self {
        Self {
            interactables: Default::default(),
            focused:       Default::default(),

            mouse_pos:   Default::default(),
            mouse_state: ElementState::Released,
        }
    }

    pub fn tick(&mut self, evs: Vec<glutin::Event>, data: &mut Box<DataBox>) {
        for ev in evs.into_iter() {
            match ev {
                glutin::Event::WindowEvent{event, ..} =>
                    match event {
                        CursorMoved {
                            position: pos,
                            modifiers: mods, ..
                        } => self.mouse_moved(pos, mods, data),
                        MouseInput {
                            state,
                            button, ..
                        } => self.mouse_click(state, button, data),
                        _ => ()
                    },
                _ => ()
            }
        }
    }

    fn emit(item: &Rc<RefCell<dyn Interactable>>, ev: Event, db: &mut Box<DataBox>) {
        item.borrow_mut().event(ev, db);
    }

    #[inline]
    fn mouse_moved(&mut self, pos: LogicalPosition, mods: ModifiersState, data: &mut Box<DataBox>) {
        let new_pos = pos.to_physical(data.dpi).into();

        for item in self.interactables.iter() {
            if item.borrow().is_inside(new_pos) {
                Self::emit(item, Event::MouseMove(new_pos), data);
            }

            if item.borrow().is_inside(new_pos) && !item.borrow().is_inside(self.mouse_pos) {
                Self::emit(item, Event::MouseEnter(new_pos), data);
            }else if !item.borrow().is_inside(new_pos) && item.borrow().is_inside(self.mouse_pos) {
                Self::emit(item, Event::MouseLeave, data);
            }
        }

        self.mouse_pos = new_pos;
    }

    #[inline]
    fn mouse_click(&mut self, state: ElementState, button: MouseButton, data: &mut Box<DataBox>) {
        for item in self.interactables.iter() {
            if item.borrow().is_inside(self.mouse_pos) {
                match state {
                    ElementState::Pressed  => 
                        Self::emit(item, Event::MouseClick(self.mouse_pos), data),
                    ElementState::Released =>
                        Self::emit(item, Event::MouseLift(self.mouse_pos), data),
                }
            }
        }
    }

    pub fn register(&mut self, int: Rc<RefCell<dyn Interactable>>) {
        self.interactables.push(int);
    }
}
