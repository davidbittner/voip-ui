use crate::context::render::*;
use crate::context::DataBox;
use crate::context::interact::*;

use derive_builder::*;

#[derive(Builder, Clone)]
#[builder(setter(into))]
pub struct Label {
    #[builder(default)]
    pub text:  String,
    #[builder(default = "nanovg::Color::from_rgb(230, 230, 230)")]
    color: nanovg::Color,
    #[builder(default)]
    pub pos:   Vec2d,
    #[builder(default = "16.0")]
    text_size:  f32,
    #[builder(default = "nanovg::Alignment::new()")]
    alignment: nanovg::Alignment,
}

impl Renderable for Label {
    fn render(&self, frame: &mut nanovg::Frame, data: &DataBox) {
        let transform = nanovg::Transform::new()
            .with_translation(self.pos.x as f32, self.pos.y as f32);

        let options = nanovg::TextOptions {
            size:  self.text_size,
            color: self.color,
            align: self.alignment,
            transform: Some(transform),
            .. Default::default()
        };

        frame.text(
            data.font,
            (0.0, 0.0),
            &self.text,
            options
        );

    }

    fn resize(&mut self, space: Size) {
        self.pos = Vec2d {
            x: space.pos.x + space.siz.x/2.0,
            y: space.pos.y + space.siz.y/2.0
        };
    }
}
