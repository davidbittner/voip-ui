use crate::context::render::*;
use crate::context::interact::*;

use crate::context::DataBox;

pub type CallbackType = Box<FnMut(Event, &mut DataBox) -> ()>;

pub struct Button
{
    label:    super::Label,
    last_col: Option<nanovg::Color>,
    color:    nanovg::Color,
    dims:     Size,

    callback: CallbackType
}

impl Button
{
    pub fn new<T: Into<Size>>(dims: T, text: String, callback: CallbackType) -> Self {
        let dims: Size = dims.into();
        
        let w = dims.siz.x/2.0;
        let h = dims.siz.y/2.0;

        let label = super::LabelBuilder::default()
            .text(text.clone())
            .pos(Vec2d {
                x: dims.pos.x + w,
                y: dims.pos.y + h
            })
            .alignment(nanovg::Alignment::new()
                       .center()
                       .middle())
            .build()
            .unwrap();

        Button {
            dims: dims,
            label: label,
            last_col: None,
            color: nanovg::Color::from_rgb(30, 30, 30),
            callback: callback,
        }
    }
}

impl Renderable for Button
{
    fn render(&self, frame: &mut nanovg::Frame, data: &DataBox) {
        let transform = nanovg::Transform::new()
            .with_translation(self.dims.pos.x as f32, self.dims.pos.y as f32);
        
        frame.path(
            |path| {
                path.rect((0.0, 0.0), self.dims.siz.into());
                path.fill(self.color, Default::default());
            },
            nanovg::PathOptions {
                transform: Some(transform),
                .. Default::default()
            }
        );

        self.label.render(frame, data);
    }

    fn resize(&mut self, space: Size) {
        self.dims = space;
        self.label.resize(space);
    }
}

impl Interactable for Button
{
    fn get_bounds(&self) -> Size {
        self.dims
    }

    fn event(&mut self, ev: Event, db: &mut Box<DataBox>) {
        match ev {
            Event::MouseEnter(_) => {
                self.last_col = Some(self.color);
                self.color    = nanovg::Color::from_rgb(80, 80, 80);
            },
            Event::MouseLeave    => {
                self.last_col = Some(self.color);
                self.color    = nanovg::Color::from_rgb(30, 30, 30);
            },
            Event::MouseClick(_) => {
                self.last_col = Some(self.color);
                self.color    = nanovg::Color::from_rgb(100, 100, 100);
            },
            Event::MouseLift(_) => {
                self.color = self
                    .last_col
                    .unwrap_or(nanovg::Color::from_rgb(30, 30, 30));
            },
            _ => ()
        }
        
        (self.callback)(ev, db);
    }
}
