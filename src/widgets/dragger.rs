use crate::context::render::Renderable;
use crate::context::databox::DataBox;
use crate::context::interact::Interactable;

use crate::context::interact::Size;
use crate::context::interact::Vec2d;
use crate::context::interact::Event;

use glutin::MouseCursor;

pub struct Dragger<A, B> 
    where
        A: Interactable + Renderable,
        B: Interactable + Renderable,
{
    left:  A,
    right: B,

    bar_wid: f32,
    padding: u32,
    perc:    f32,

    dragging: bool,

    dims: Size,

    color: nanovg::Color,
}

impl<A, B> Dragger<A, B>
    where
        A: Interactable + Renderable,
        B: Interactable + Renderable,
{
    pub fn new(a: A, b: B) -> Self {
        Self {
            left: a,
            right: b,

            bar_wid: 7.0,
            perc: 20.0,

            padding: 4,

            dragging: false,
            dims: Default::default(),

            color: nanovg::Color::from_rgb(60, 60, 60),
        }
    }

    fn is_middle_bar(&self, pos: &Vec2d) -> bool {
        let middle_height = 20.0;
        let middle_wid = 3.0;

        let x: f32 = (self.dims.siz.x * (self.perc/100.0) as f32) - (self.bar_wid/2.0);

        let middle_bar = Size {
            pos: (x, 0.0).into(),
            siz: (self.bar_wid, self.dims.siz.y).into()
        };

        middle_bar.is_inside(pos)
    }
}

impl<A, B> Renderable for Dragger<A, B> 
    where
        A: Interactable + Renderable,
        B: Interactable + Renderable,
{
    fn render(&self, frame: &mut nanovg::Frame, data: &DataBox) {
        let transform = nanovg::Transform::new()
            .with_translation(self.dims.pos.x, self.dims.pos.y);

        let x: f32 = self.dims.siz.x * (self.perc/100.0) as f32;

        let middle_height = 20.0;
        let middle_wid = 3.0;

        self.left.render(frame, data);
        self.right.render(frame, data);

        frame.path(
            |path| {
                path.rect(
                    (x as f32 - (self.bar_wid/2.0), 0.0),
                    (self.bar_wid, self.dims.siz.y as f32)
                );

                path.fill(self.color, Default::default());

            },
            nanovg::PathOptions {
                transform: Some(transform),
                .. Default::default()
            }
        );

        frame.path(
            |path| {
                let middle_x = x - (middle_wid/2.0);
                path.rect(
                    (middle_x, (self.dims.siz.y/2.0) - middle_height),
                    (middle_wid, middle_height)
                );
                path.fill(nanovg::Color::from_rgb(200, 200, 200), Default::default());
            },
            nanovg::PathOptions {
                transform: Some(transform),
                .. Default::default()
            }
        );

    }

    fn resize(&mut self, space: Size) {
        self.dims = space;

        let left_wid  = (self.dims.siz.x * (self.perc/100.0)) - (self.padding*2) as f32;
        let right_wid = (self.dims.siz.x * ((100.0 - self.perc)/100.0)) - (self.padding*2) as f32;

        let hei = self.dims.siz.y - (self.padding*2) as f32;

        self.left.resize(Size {
            pos: Vec2d {
                x: self.padding as f32,
                y: self.padding as f32
            },
            siz: Vec2d {
                x: left_wid,
                y: hei
            }
        });

        self.right.resize(Size {
            pos: Vec2d {
                x: left_wid + self.padding as f32,
                y: self.padding as f32
            },
            siz: Vec2d {
                x: right_wid,
                y: hei
            }
        });
    }

}

impl<A, B> Interactable for Dragger<A, B> 
    where
        A: Interactable + Renderable,
        B: Interactable + Renderable,
{
    fn get_bounds(&self) -> Size {
        self.dims
    }

    fn event(&mut self, ev: Event, db: &mut Box<DataBox>) {
        match ev {
            Event::MouseClick(pos) => {
                if self.is_middle_bar(&pos) {
                    self.dragging = true;
                    db.cursor = Some(MouseCursor::Grabbing);
                }else{
                    if self.left.is_inside(pos) {
                        self.left.event(ev, db);
                    }else{
                        self.right.event(ev, db);
                    }
                }
            },
            Event::MouseMove(pos) => {
                if self.dragging {
                    self.perc = (pos.x / self.dims.siz.x)*100.0;
                    self.resize(self.dims);
                }else {
                    if self.is_middle_bar(&pos) {
                        db.cursor = Some(MouseCursor::Grab);
                        self.color = nanovg::Color::from_rgb(100, 100, 100);
                    }else{
                        self.color = nanovg::Color::from_rgb(60, 60, 60);
                        db.cursor = Some(MouseCursor::Default);

                        if self.left.is_inside(pos) {
                            self.left.event(ev, db);
                        }else{
                            self.right.event(ev, db);
                        }
                    }
                }
            },
            Event::MouseLift(pos) => {
                if self.dragging {
                    if self.is_middle_bar(&pos) {
                        db.cursor = Some(MouseCursor::Grab);
                    }else {
                        db.cursor = Some(MouseCursor::Default);
                    }
                    self.dragging = false;
                }else{
                    if self.left.is_inside(pos) {
                        self.left.event(ev, db);
                    }else{
                        self.right.event(ev, db);
                    }
                }
            },
            _ => ()
        }
    }
}
