use crate::context::render::Renderable;
use crate::context::DataBox;

use crate::context::interact::{Size, Vec2d};

use derive_builder::*;

#[derive(Builder)]
#[builder(setter(into))]
pub struct TextBox {
    #[builder(default)]
    pub text:  String,
    #[builder(default = "nanovg::Color::from_rgb(0, 0, 0)")]
    color: nanovg::Color,
    #[builder(default)]
    pub pos:   Vec2d,
    #[builder(default = "16.0")]
    text_size:  f32,
    #[builder(default = "nanovg::Alignment::new()")]
    alignment: nanovg::Alignment,
    pub bound_size: Vec2d,
}

impl Renderable for TextBox {
    fn render(&self, frame: &mut nanovg::Frame, data: &DataBox) {
        let transform = nanovg::Transform::new()
            .with_translation(self.pos.x as f32, self.pos.y as f32);

        let options = nanovg::TextOptions {
            size:  self.text_size,
            color: self.color,
            align: self.alignment,
            transform: Some(transform),
            .. Default::default()
        };

        let metrics = frame.text_metrics(data.font, options);
        let mut counter = 0;
        for row in frame.text_break_lines(&self.text, self.bound_size.x as f32) {
            frame.text(
                data.font,
                (0.0, counter as f32 * metrics.line_height),
                &row.text,
                options
            );

            counter+=1;
        }
    }

    fn resize(&mut self, space: Size) {

    }
}
